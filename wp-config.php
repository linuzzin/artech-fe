<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache


/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file viene utilizzato, durante l’installazione, dallo script
 * di creazione di wp-config.php. Non è necessario utilizzarlo solo via web
 * puoi copiare questo file in «wp-config.php» e riempire i valori corretti.
 *
 * Questo file definisce le seguenti configurazioni:
 *
 * * Impostazioni MySQL
 * * Chiavi Segrete
 * * Prefisso Tabella
 * * ABSPATH
 *
 * * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - È possibile ottenere queste informazioni dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define( 'DB_NAME', 'didatticaprogettoartech' );

/** Nome utente del database MySQL */
define( 'DB_USER', 'silem' );

/** Password del database MySQL */
define( 'DB_PASSWORD', 's1l3m!' );

/** Hostname MySQL  */
define( 'DB_HOST', 'localhost' );

/** Charset del Database da utilizzare nella creazione delle tabelle. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * È possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * È possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ciò forzerà tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'g)xG[+vWb8C<{5m);[2o1]Hu$`9(vp#@k2BPJFqHFrc<kDQw^$+7E=wtW.:!oQ3*' );
define( 'SECURE_AUTH_KEY',  '9P> jYh8#3:1? !i2melPjAX|)I(+++IJ@=xa;x.Nt@lB,VyxAoMclug>BZW%|za' );
define( 'LOGGED_IN_KEY',    'qbz>pKd!.-ASu?;IN9a}~FRRBR{ ^%Vhq&n.Z;53uI0**q1DbjxujicguFZK&Iom' );
define( 'NONCE_KEY',        'AHN%dKm ]0*ASh E6I)@xx|16Nt}jy5-[t1V!PiTf),P]-zOf9aadZeD&Vmf|5A6' );
define( 'AUTH_SALT',        '^3%Aq/QBat(WA^Z<?DV%}Bp1$-hF5(NO2/EtM+OsoJN<a_X+L8L:uc_0.&@L4A({' );
define( 'SECURE_AUTH_SALT', 'ALy$$>/]CWD5hm q-pZ4~}GY.]:F3c<IV.&o. 8+~_Z<3KKAo;6-m)]jJ8<:`[^S' );
define( 'LOGGED_IN_SALT',   'UKWI`lp0X$2tX_LV~-Q#[Uz}B^K+Q{A7{PgLa#_9SH_P|YaL^}AY)i&2MABnx|<P' );
define( 'NONCE_SALT',       ']OiVp5F]{1nS$NoDG}L STdOcl1MJf.yU=QjK^}cXzQnn{.e2-a?dwk4&%5{m7Fm' );

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress.
 *
 * È possibile avere installazioni multiple su di un unico database
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix = 'wp_';

/**
 * Per gli sviluppatori: modalità di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi durante lo sviluppo
 * È fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all’interno dei loro ambienti di sviluppo.
 *
 * Per informazioni sulle altre costanti che possono essere utilizzate per il debug,
 * leggi la documentazione
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', false);



define( 'ACF_PRO_KEY', 'b3JkZXJfaWQ9NzQ2MTV8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE2LTAyLTA5IDExOjQ5OjE5' );


/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');
