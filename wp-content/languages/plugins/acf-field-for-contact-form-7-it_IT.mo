��          �      \      �  7   �     	  O   &     v     �     �     �     �     �  	   �     �     �     �     �            *   9  ;   d     �  5  �  <   �     .  S   K     �     �     �     �     �     �  	              #     7     H     c  "   g  $   �  ;   �     �                  	                                                                          
       "%s" needs "%s" to run. Please download and activate it ACF Field For Contact Form 7 Adds a new 'Contact Form 7' field to the popular Advanced Custom Fields plugin. Allow Null? Contact Form 7 Contact form 7 Disabled Forms: Disabled forms: Hide disabled forms? KrishaWeb No Select Multiple? Select form Select multiple forms? Yes You can not select these forms You will not be able to select these forms https://wordpress.org/plugins/acf-field-for-contact-form-7/ https://www.krishaweb.com/ PO-Revision-Date: 2020-06-19 16:52:19+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: it
Project-Id-Version: Plugins - ACF Field For Contact Form 7 - Stable (latest release)
 "%s" ha bisogno di "%s" per funzionare. Scaricalo e attivalo ACF Field For Contact Form 7 Aggiunge un nuovo campo 'Contact Form 7' al popolare plugin Advanced Custom Fields. Consentire null? Contact Form 7 Contact Form 7 Moduli disabilitati: Moduli disabilitati: Nascondi moduli disabilitati? KrishaWeb No Seleziona multipli? Seleziona modulo Seleziona moduli multipli? Sì Non puoi selezionare questi moduli Non potrai selezionare questi moduli https://wordpress.org/plugins/acf-field-for-contact-form-7/ https://www.krishaweb.com/ 