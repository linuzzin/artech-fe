<?php

function theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'avada-stylesheet' ) );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function avada_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
}
add_action( 'after_setup_theme', 'avada_lang_setup' );



add_shortcode('custom_user_registration_account', 'custom_user_registration_account'); 

// Shortcode Demo with simple <h2> tag
function custom_user_registration_account($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
	$all_meta_for_user = get_user_meta(  get_current_user_id() );
	$return = "<h2 style=\"text-align: left;\"><strong>Gentile</strong> ".$all_meta_for_user["first_name"][0]." ".$all_meta_for_user["last_name"][0].", <strong>le diamo il benvenuto in Didattica Progetto Artech </strong>(DPA).";
	$return .= "<br>La <strong>piattaforma digitale</strong> rivolta alle scuole di ogni ordine e grado, con contenuti differenti per tipologia di grado di istituto.";

	$return .= do_shortcode("[user_registration_my_account]");

    return $return;
}

//custom_user shorcodes
function get_user_name()
{
	$name = get_user_meta(get_current_user_id(), 'first_name', true );
	return $name;
}
add_shortcode('user_name', 'get_user_name');

function get_user_surname()
{
	$surname = get_user_meta(get_current_user_id(), 'last_name', true );
	return $surname;
}
add_shortcode('user_surname', 'get_user_surname');

function get_user_email()
{
  	$user_info = get_userdata(get_current_user_id());
	$email = $user_info->user_email;
	return $email;
}
add_shortcode('user_email', 'get_user_email');

function get_user_role()
{
	$role = get_user_meta(get_current_user_id(), 'user_registration_input_box_1604855944', true);
	return $role;
}
add_shortcode('user_role', 'get_user_role');

function get_user_nome_istituto(){
	$nome_istituto = get_user_meta(get_current_user_id(), 'user_registration_input_box_nome_istituto', true);
	return $nome_istituto;
}
add_shortcode('user_nome_istituto', 'get_user_nome_istituto');

function get_user_indirizzo_plesso(){
	$indirizzo_plesso = get_user_meta(get_current_user_id(), 'user_registration_input_box_indirizzo', true);
	return $indirizzo_plesso;
}
add_shortcode('user_indirizzo_plesso', 'get_user_indirizzo_plesso');

function get_user_citta_istituto(){
	$indirizzo_istituto = get_user_meta(get_current_user_id(), 'user_registration_input_box_citta', true);
	return $indirizzo_istituto;
}
add_shortcode('user_citta_istituto', 'get_user_citta_istituto');

function get_user_cap_istituto(){
	$cap_istituto = get_user_meta(get_current_user_id(), 'user_registration_input_box_cap', true);
	return $cap_istituto;
}
add_shortcode('user_cap_istituto', 'get_user_cap_istituto');

function post_title_shortcode(){
    return get_the_title();
}
add_shortcode('post_title','post_title_shortcode');


add_action( 'wp', 'redirect_non_logged_users_to_specific_page' );

function redirect_non_logged_users_to_specific_page() {
	$actual_link = strtok("https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", '?');
	$vettUrlEnabled = [
	"https://www.didatticaprogettoartech.com/",
	"https://www.didatticaprogettoartech.com/arte-e-musei-video-lezioni/",
	"https://www.didatticaprogettoartech.com/cinema-video-lezioni/",
	"https://www.didatticaprogettoartech.com/docenti/",
	"https://www.didatticaprogettoartech.com/docenti/vittorio-rifranti/",
	"https://www.didatticaprogettoartech.com/docenti/marco-longo/",
	"https://www.didatticaprogettoartech.com/docenti/andrea-caccia/",
	"https://www.didatticaprogettoartech.com/docenti/mauro-gervasini/",
	"https://www.didatticaprogettoartech.com/docenti/mario-addis/",
	"https://www.didatticaprogettoartech.com/docenti/bruno-di-marino/",
	"https://www.didatticaprogettoartech.com/abbonamento/",
	"https://www.didatticaprogettoartech.com/registrazione/",
	"https://www.didatticaprogettoartech.com/news",
	"https://www.didatticaprogettoartech.com/news/scopri-artech-card/",
	"https://www.didatticaprogettoartech.com/news/musei-eventi-speciali-in-live-streaming/",
	"https://www.didatticaprogettoartech.com/news/scopri-i-webinar/",
	"https://www.didatticaprogettoartech.com/strumenti-per-i-docenti/",
	"https://www.didatticaprogettoartech.com/cinema-arte-musei/",
	"https://www.didatticaprogettoartech.com/newsletter",
	"https://www.didatticaprogettoartech.com/contatti/",
	"https://www.didatticaprogettoartech.com/welcome/",
	"https://www.didatticaprogettoartech.com/wp-login.php",
	"https://www.didatticaprogettoartech.com/wp-admin/admin-ajax.php"
	];
	
	$visible = false;
	if (is_single() && !empty(get_field("pagina_pubblica"))) $visible = true;
	if (in_array($actual_link, $vettUrlEnabled)) $visible = true;
	if (is_user_logged_in()) $visible = true;

	if ( !$visible ) {
		wp_redirect( 'https://www.didatticaprogettoartech.com/welcome/', 301 ); 
		exit;
   }

}
