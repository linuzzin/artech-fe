<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/user-registration/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion UserRegistration will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.wpeverest.com/user-registration/template-structure/
 * @author  WPEverest
 * @package UserRegistration/Templates
 * @version 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<div class="fusion-text fusion-text-3"><h3 class="account-section-heading"><span style="color: #f7931e;"><strong>PROFILO DIDATTICO</strong></span></h3></div>
<?php
//$all_meta_for_user = get_user_meta(  get_current_user_id() );
//print_r( $all_meta_for_user );
$tipo_scuola = get_user_meta( get_current_user_id(), 'user_registration_check_box_tipi_scuola');
$box_pacchetti = get_user_meta( get_current_user_id(), 'user_registration_check_box_pacchetti');
//print_r($box_pacchetti[0]);

foreach($box_pacchetti[0] as $scuola){
	?>
	
	<?
	if($scuola == "Scuola primaria") {
		$linkCinema = "https://www.didatticaprogettoartech.com/cinema-scuola-primaria/";
		$linkArte = "https://www.didatticaprogettoartech.com/arte-e-musei-scuola-primaria/";
		$linkWebinar = "https://www.didatticaprogettoartech.com/webinar-scuola-primaria/";
	}
	if($scuola == "Scuola secondaria di primo grado") {
		$linkCinema = "https://www.didatticaprogettoartech.com/cinema-secondaria-di-primo-grado/";
		$linkArte = "https://www.didatticaprogettoartech.com/arte-e-musei-secondaria-di-primo-grado/";
		$linkWebinar = "https://www.didatticaprogettoartech.com/webinar-secondaria-di-primo-grado/";
	}
	if($scuola == "Scuola secondaria di secondo grado") {
		$linkCinema = "https://www.didatticaprogettoartech.com/cinema-secondaria-di-secondo-grado/";
		$linkArte = "https://www.didatticaprogettoartech.com/arte-e-musei-secondaria-di-secondo-grado/";
		$linkWebinar = "https://www.didatticaprogettoartech.com/webinar-secondaria-di-secondo-grado/";
	}
	if($scuola == "WEBINAR") {
		$link = "https://www.didatticaprogettoartech.com/my-webinar/";
	}
	if($scuola == "LEZIONI") {
		$link = "https://www.didatticaprogettoartech.com/my-lezioni/";
	}
	if($scuola == "INCONTRI CON GLI ESPERTI") {
		$link = "https://www.didatticaprogettoartech.com/my-esperti/";
	}
	

	if($scuola == "Scuola primaria" || $scuola == "Scuola secondaria di primo grado" || $scuola == "Scuola secondaria di secondo grado" ) {
	?>
	<div class="fusion-text fusion-text-14"><p>Il <strong>piano didattico</strong> dedicato alla <strong><span style="color: #f7931e;"><?php echo $scuola;?></span></strong>.</p>
	<p>Selezioni <strong>l’argomento</strong> che desidera approfondire:</p></div>
		<div class="fusion-builder-row fusion-builder-row-inner fusion-row fusion-flex-align-items-flex-start" style="width:104% !important;max-width:104% !important;margin-left: calc(-4% / 2 );margin-right: calc(-4% / 2 );">
				<div class="fusion-layout-column fusion_builder_column_inner fusion-builder-nested-column-0 fusion_builder_column_inner_1_3 1_3 fusion-flex-column"><div class="fusion-column-wrapper fusion-flex-justify-content-flex-start fusion-content-layout-column" style="background-position: left top; background-repeat: no-repeat; background-size: cover; padding: 0px; min-height: 0px;"><div><style type="text/css">.fusion-button.button-1 {border-radius:4px;}</style>
				<a href="<?php echo $linkCinema;?>" class="fusion-button button-flat fusion-button-default-size button-default button-1 fusion-button-default-span fusion-button-default-type" target="_self"><i class="fa-film fas button-icon-left" aria-hidden="true"></i><span class="fusion-button-text">CINEMA</span></a></div></div></div><style type="text/css">.fusion-body .fusion-builder-nested-column-0{width:33.333333333333% !important;margin-top : 0px;margin-bottom : 20px;}.fusion-builder-nested-column-0 > .fusion-column-wrapper {padding-top : 0px !important;padding-right : 0px !important;margin-right : 5.76%;padding-bottom : 0px !important;padding-left : 0px !important;margin-left : 5.76%;}@media only screen and (max-width:1024px) {.fusion-body .fusion-builder-nested-column-0{width:100% !important;order : 0;}.fusion-builder-nested-column-0 > .fusion-column-wrapper {margin-right : 1.92%;margin-left : 1.92%;}}@media only screen and (max-width:640px) {.fusion-body .fusion-builder-nested-column-0{width:100% !important;order : 0;}.fusion-builder-nested-column-0 > .fusion-column-wrapper {margin-right : 1.92%;margin-left : 1.92%;}}</style><div class="fusion-layout-column fusion_builder_column_inner fusion-builder-nested-column-1 fusion_builder_column_inner_1_3 1_3 fusion-flex-column"><div class="fusion-column-wrapper fusion-flex-justify-content-flex-start fusion-content-layout-column" style="background-position: left top; background-repeat: no-repeat; background-size: cover; padding: 0px; min-height: 0px;"><div><style type="text/css">.fusion-button.button-2 {border-radius:4px;}</style>
				<a href="<?php echo $linkArte;?>" class="fusion-button button-flat fusion-button-default-size button-default button-2 fusion-button-default-span fusion-button-default-type" target="_self"><i class="fa-globe fas button-icon-left" aria-hidden="true"></i><span class="fusion-button-text">ARTE/MUSEI</span></a></div></div></div><style type="text/css">.fusion-body .fusion-builder-nested-column-1{width:33.333333333333% !important;margin-top : 0px;margin-bottom : 20px;}.fusion-builder-nested-column-1 > .fusion-column-wrapper {padding-top : 0px !important;padding-right : 0px !important;margin-right : 5.76%;padding-bottom : 0px !important;padding-left : 0px !important;margin-left : 5.76%;}@media only screen and (max-width:1024px) {.fusion-body .fusion-builder-nested-column-1{width:100% !important;order : 0;}.fusion-builder-nested-column-1 > .fusion-column-wrapper {margin-right : 1.92%;margin-left : 1.92%;}}@media only screen and (max-width:640px) {.fusion-body .fusion-builder-nested-column-1{width:100% !important;order : 0;}.fusion-builder-nested-column-1 > .fusion-column-wrapper {margin-right : 1.92%;margin-left : 1.92%;}}</style><div class="fusion-layout-column fusion_builder_column_inner fusion-builder-nested-column-2 fusion_builder_column_inner_1_3 1_3 fusion-flex-column"><div class="fusion-column-wrapper fusion-flex-justify-content-flex-start fusion-content-layout-column" style="background-position: left top; background-repeat: no-repeat; background-size: cover; padding: 0px; min-height: 0px;"><div><style type="text/css">.fusion-button.button-3 {border-radius:4px;}</style>
				<a href="<?php echo $linkWebinar;?>" class="fusion-button button-flat fusion-button-default-size button-default button-3 fusion-button-default-span fusion-button-default-type" target="_self"><i class="fa-chalkboard-teacher fas button-icon-left" aria-hidden="true"></i><span class="fusion-button-text">WEBINAR</span></a></div></div></div><style type="text/css">.fusion-body .fusion-builder-nested-column-2{width:33.333333333333% !important;margin-top : 0px;margin-bottom : 20px;}.fusion-builder-nested-column-2 > .fusion-column-wrapper {padding-top : 0px !important;padding-right : 0px !important;margin-right : 5.76%;padding-bottom : 0px !important;padding-left : 0px !important;margin-left : 5.76%;}@media only screen and (max-width:1024px) {.fusion-body .fusion-builder-nested-column-2{width:100% !important;order : 0;}.fusion-builder-nested-column-2 > .fusion-column-wrapper {margin-right : 1.92%;margin-left : 1.92%;}}@media only screen and (max-width:640px) {.fusion-body .fusion-builder-nested-column-2{width:100% !important;order : 0;}.fusion-builder-nested-column-2 > .fusion-column-wrapper {margin-right : 1.92%;margin-left : 1.92%;}}</style></div>
		<div class="fusion-separator-border sep-single sep-solid" style="border-color:#e6e6e6;border-top-width:2px;"></div>
		<p></p>
		<?php
	}else{
		?><div class="fusion-text fusion-text-14"><p>Il <strong>piano didattico</strong> dedicato alla <strong><span style="color: #f7931e;"><?php echo $scuola;?></span></strong>.</p>
			</div>
		
		<div><style type="text/css">.fusion-button.button-1 {border-radius:4px;}</style>
		<a href="<?php echo $link;?>" class="fusion-button button-flat fusion-button-default-size button-default button-1 fusion-button-default-span fusion-button-default-type" target="_self"><i class="fa-film fas button-icon-left" aria-hidden="true"></i><span class="fusion-button-text"><?php echo $scuola;?></span></a></div>
		<p></p>
		<?php
	}
}

?>


<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'user_registration_account_dashboard' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
