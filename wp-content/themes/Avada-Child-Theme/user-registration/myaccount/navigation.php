<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/user-registration/myaccount/navigation.php.
 *
 * HOWEVER, on occasion UserRegistration will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.wpeverest.com/user-registration/template-structure/
 * @author  WPEverest
 * @package UserRegistration/Templates
 * @version 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

do_action( 'user_registration_before_account_navigation' );
?>

<div class="um-account-side uimob340-hide uimob500-hide">
<ul>

<li>
<a data-tab="general" href="https://www.didatticaprogettoartech.com/welcome/" class="um-account-link current">
<span class="um-account-icontip uimob800-show um-tip-w" original-title="Profilo didattico"><i class="um-faicon-user"></i></span>
<span class="um-account-icon uimob800-hide"><img style="position: absolute; top: 7px;left: 9px;" src="/wp-content/uploads/2021/02/logo_user.png" width="15px" height="15px" alt="logo user"></span>
<span class="um-account-title uimob800-hide">Profilo didattico</span>
<span class="um-account-arrow uimob800-hide"><i class="um-faicon-angle-right"></i></span>
</a>
</li>

<li>
<a data-tab="privacy" href="https://www.didatticaprogettoartech.com/welcome/edit-profile/" class="um-account-link ">
<span class="um-account-icontip uimob800-show um-tip-w" original-title="modifica profilo"><i class="um-faicon-lock"></i></span>
<span class="um-account-icon uimob800-hide"><img style="position: absolute; top: 7px;left: 9px;" src="/wp-content/uploads/2021/02/privacy.png" width="15px" height="15px" alt="logo user"></span>
<span class="um-account-title uimob800-hide">Modifica profilo</span>
<span class="um-account-arrow uimob800-hide"><i class="um-faicon-angle-right"></i></span>
</a>
</li>


<li>
<a data-tab="password" href="https://www.didatticaprogettoartech.com/welcome/edit-password/" class="um-account-link ">
<span class="um-account-icontip uimob800-show um-tip-w" original-title="cambia Password"><i class="um-faicon-asterisk"></i></span>
<span class="um-account-icon uimob800-hide"><img style="position: absolute; top: 7px;left: 9px;" src="/wp-content/uploads/2021/02/psw.png" width="15px" height="15px" alt="logo user"></span>
<span class="um-account-title uimob800-hide">Cambia Password</span>
<span class="um-account-arrow uimob800-hide"><i class="um-faicon-angle-right"></i></span>
</a>
</li>

<li>
<a data-tab="delete" href="https://www.didatticaprogettoartech.com/welcome/user-logout/" class="um-account-link ">
<span class="um-account-icontip uimob800-show um-tip-w" original-title="Logout"><i class="um-faicon-trash-o"></i></span>
<span class="um-account-icon uimob800-hide"><img style="position: absolute; top: 7px;left: 9px;" src="/wp-content/uploads/2021/02/logout.png" width="15px" height="15px" alt="logo user"></span>
<span class="um-account-title uimob800-hide">Logout</span>
<span class="um-account-arrow uimob800-hide"><i class="um-faicon-angle-right"></i></span>
</a>
</li>

</ul>
</div>

<?php do_action( 'user_registration_after_account_navigation' ); ?>
<style>
/*
	- Layout
*/

.um-account i {
	vertical-align: baseline !important;
}

.um-account p.um-notice {
	margin: 0 0 20px 0 !important;
	padding: 12px 15px !important;
}

.um-account a:focus {
    outline: 0 !important; /*removes the dotted border*/
}

.um-account-side {
	float: left;
	width: 25%;
	padding: 0px;
	box-sizing: border-box;
}

.um-account-main {
	float: left;
	width: 70%;
	padding: 0px 0px 0px 1.5em;
	box-sizing: border-box;
}

.um-account-main a {
	border-bottom: none !important;
}

.um-account-tab {
	display: none;
}

/*
	- Main tab
*/

.um-account-main div.um-account-heading {
	display: flex;
	flex-wrap: nowrap;
	flex-direction: row;
	justify-content: flex-start;
	align-items: baseline;
	margin: 0 !important;
	font-size: 18px;
	line-height: 18px;
	font-weight: bold;
	color: #555;
}

.um-account-main div.um-account-heading i {
	margin-right: 10px;
	font-size: 26px;
	position: relative;
	top: 2px;
}

.um-account-main p {
	margin: 20px 0 0 0!important;
	padding: 0 !important;
}

.um-account-main label {
	font-size: 15px;
}

/*
	- Account photo
*/

.um-account-meta {
	text-align: center;
	margin-bottom: 20px;
}

.um-account-meta img {
	margin: 0 !important;
	position: static !important;
	float: none !important;
	display: inline-block;
}

.um-account-meta.radius-1 img { 	-moz-border-radius: 999px;-webkit-border-radius: 999px;border-radius: 999px }
.um-account-meta.radius-2 img { 	-moz-border-radius: 4px;-webkit-border-radius: 4px;border-radius: 4px }
.um-account-meta.radius-3 img { 	-moz-border-radius: 0;-webkit-border-radius: 0;border-radius: 0 }

.um-account-name {
	padding-top: 12px;
}

.um-account-name a {
	font-weight: bold;
	color: #555;
	text-decoration: none !important;
	font-size: 18px;
	line-height: 1.4em;
}

.um-account-profile-link a {
	font-size: 13px;
	font-weight: normal;
}

/*
	- Account nav
*/

.um-account-nav a {
	display: block;
	height: 44px;
	line-height: 44px;
	color: #666 !important;
	text-decoration: none !important;
	position: relative;
	padding-left: 40px;
	border-bottom: 1px solid #eee !important;
	transition: all .2s linear;
	font-size: 14px;
}

.um-account-nav a.current{
	font-weight: bold;
}

.um-account-nav span.arr {
	position: absolute;
	right: 0;
	top: 1px;
	font-size: 28px;
}

.um-account-nav span.ico {
	position: absolute;
	left: 0;
	top: 0;
	font-size: 21px;
	width: 21px;
	text-align: center;
}

/*
	- Account tabs
*/

.um-account-side ul, .um-account-side li {
	margin: 0 !important;
	padding: 0 !important;
	list-style-type: none !important;
}

.um-account-side li {margin-bottom: 1px !important;background: #eee;}

.um-account-side li a{
	display: block;
	padding: 4px 0px;
	font-size: 14px;
	height: 30px;
	line-height: 20px;
	color: #999;
	position: relative;
}

.um-account-side li a span.um-account-arrow {
	position: absolute;
	right: 10px;
	top: 6px;
	font-size: 26px;
	opacity: 0.6;
}

.um-account-side li a span.um-account-icon,
.um-account-side li a.current span.um-account-icon,
.um-account-side li a.current:hover span.um-account-icon
{
	text-align: center;
	font-size: 20px;
	width: 20px;
	height: 30px;
	border-right: 1px solid #ccc;
	color: #444;
	float: left;
	padding: 0px 15px;
	font-weight: normal !important;
}

.um-account-side li a span.um-account-icon i {
	display: block;
	height: 30px;
	line-height: 30px;
}

.um-account-side li a span.um-account-title {
	padding-left: 20px;
	float: left;
	height: 30px;
	line-height: 30px;
	color: #555;
}

.um-account-side li a:hover {color: #444; background: #ddd}

.um-account-side li a.current,
.um-account-side li a.current:hover {
	color: #444;
	font-weight: bold;
	text-decoration: none !important;
}

.um-field-export_data .um-field-error {
	display: none;
}

.um-field-export_data .um-field-area-response {
	display: none;
	line-height: 1.5;
	padding: 10px 0;
}

.um-request-button {
	display: inline-block;
	background-color: #3ba1da;
	border-radius: 5px;
	color: #fff;
	margin: 10px 0 0;
	padding: 5px 10px;
	text-decoration: none;
}

.um-request-button:hover {
	background-color: #44b0ec;
	color: #fff;
	text-decoration: none;
}
</style>