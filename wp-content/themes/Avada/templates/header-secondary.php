<?php
/**
 * Template for the secondary header.
 *
 * @author     ThemeFusion
 * @copyright  (c) Copyright by ThemeFusion
 * @link       https://theme-fusion.com
 * @package    Avada
 * @subpackage Core
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php

$content_1 = avada_secondary_header_content( 'header_left_content' );
$content_2 = avada_secondary_header_content( 'header_right_content' );
?>

<div class="fusion-secondary-header">
	<div class="fusion-row">
		<?php if ( $content_1 ) : ?>
			<div class="fusion-alignleft">
				<?php echo $content_1; // phpcs:ignore WordPress.Security.EscapeOutput ?>
			</div>
		<?php endif; ?>
		<?php if ( $content_2 ) : ?>
			<div class="fusion-alignright">
				<?php echo $content_2; // phpcs:ignore WordPress.Security.EscapeOutput ?>
			</div>
		<?php endif; ?>
		<img src="/wp-content/uploads/2021/02/logo_user.png" width="15px" height="15px" alt="logo user">&nbsp;
		<?php if ( is_user_logged_in() ) : ?>
		<?php $obj_user = wp_get_current_user(); ?>
		<b>Ciao <a href="/welcome/"><?=ucfirst($obj_user->display_name)?></a></b>
		<?php else: ?>
		<a href="<?=get_page_link(1039)?>">Entra</a>&nbsp;|&nbsp; <a href="<?=get_page_link(1040)?>">Registrati</a>
		<?php endif; ?>
	</div>
</div>