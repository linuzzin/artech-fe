<?php
/**
 * 
 */
class Helper {


    /**
     * 
     * return the labels for taxonomies and postypes configuration
     * 
     */
    public static function generate_labels($name,$gender = 'm')
    {
        return [
            'name' => ucfirst($name), // Rename these to suit
            'singular_name' => $name,
            'add_new' => 'Aggiungi '.$name,
            'add_new_item' => 'Aggiungi '.$name,
            'edit' => 'Modifica',
            'edit_item' => 'Modifica '.$name,
            'new_item' => (($gender=='m') ? 'Nuovo ': 'Nuova ').$name,
            'view' => 'Vedi',
            'view_item' => 'Vedi '.$name,
            'search_items' => 'Cerca ' .$name,
            'not_found' => (($gender=='m') ? 'Nessun ': 'Nessuna ').$name.' '.(($gender=='m') ? 'trovato ': 'trovata '),
            'not_found_in_trash' => (($gender=='m') ? 'Nessun ': 'Nessuna ').$name.' '.(($gender=='m') ? 'trovato ': 'trovata ').' nel cestino'            
        ];
    }

    /**
     * create_post_types
     */
    public static function createPage($page) {
    
        if (get_page_by_title($page) == null) 
            // Insert the post into the database
            wp_insert_post([
                'post_title'    => $page,
                'post_content'  => 'Starter content',
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     => 'page',
                'post_name'     => $page      
            ]);
        
    }

    
    /**
     * return the tracking url built as 
     * 
     * / author / date_format / type / title;
     * 
     */
    public static function getTrackingURL($post, $type = 'single') {
        
        // take the author
        $author = strtolower( preg_replace('/\s/','-', get_the_author_meta('display_name', $post->post_author)));
        if($author != '') $author .='/';

        // get the time
        $date = get_post_time('YYYY-mm-dd', true, $post = null);
        if($date != false) $date .= '/';

        $type .= '/';

        $title = $post->post_name . '/';

        return $author . $date . $type . $title;
    } 
     

    /**
     * Returns the images sizes
     * @param post_type useful to get the sizes of every attachment
     */ 
    public static function getImageThumbSizes($post_id, $post_type = "post") {        

        if($post_type != 'attachment') $post_id = get_post_thumbnail_id( $post_id );

        return [            
            'small' => wp_get_attachment_image_url( $post_id, 'thumbnail'),
            'medium' => wp_get_attachment_image_url( $post_id, 'medium'),
            'large' => wp_get_attachment_image_url( $post_id, 'large'),
            'square' => wp_get_attachment_image_url( $post_id, 'square'),
            'medium_x2' => wp_get_attachment_image_url( $post_id, 'medium-x2'),
            'large_x2' => wp_get_attachment_image_url( $post_id, 'large-x2'),
            'square_x2' => wp_get_attachment_image_url( $post_id, 'square-x2'),
            'full' => wp_get_attachment_image_url( $post_id, 'full')
        ];   
    }

    
    public static function getCategoryRoot($post){
        $category = get_the_category($post->ID); 
        $cat_root_slug = "";
        if ($category){
            $category_parent_id = $category[0]->category_parent;
            if ( $category_parent_id != 0 ) {
                $category_parent = get_term( $category_parent_id, 'category' );
                $cat_root_slug = $category_parent;
            } else {
                $cat_root_slug = $category[0];
            }
        }
        $cat_root_slug->titolo = get_field("titolo", "category_".$cat_root_slug->term_id);
        $cat_root_slug->logo = get_field("logo", "category_".$cat_root_slug->term_id);
        return $cat_root_slug;
    }

    public static function getPrimaryTaxonomy($post,$taxonomy='category'){
        $return = [];
        if (class_exists('WPSEO_Primary_Term') && $taxonomy == 'category'){
            // Show Primary category by Yoast if it is enabled & set
            $wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, $post->ID );
            $primary_term = get_term($wpseo_primary_term->get_primary_term());
           
            if (!is_wp_error($primary_term)){
                if ( $primary_term->parent != 0 ) 
                    $return[] = get_term( $primary_term->parent, $taxonomy );
                $return[] = $primary_term;
            }
        }
        
        if (empty($return) ){
            $terms_list = get_the_terms($post->ID, $taxonomy);
            $find = false;
            if (!empty($terms_list))
                foreach($terms_list as $term)
                    if ( $term->parent != 0 ) {
                        $return[] = get_term( $term->parent, $taxonomy );
                        $return[] = $term;
                        break;
                    }
                if ($find == true) $return[] = $terms_list[0];
        }

        //pre($return,true);
        return $return;
    }


    /** Replace only first occurrence */
    public static function str_replace_first($from, $to, $content)
    {
        $from = '/'.preg_quote($from, '/').'/';

        return preg_replace($from, $to, $content, 1);
    }


    /** Replace only first occurrence */
    public static function getTerm($term)
    {
        $return = $term;
        
        if ($term->taxonomy == "area")
            $return->slug = "laurea-".$term->slug."-online-telematica";
            //pre($term,true);    
        return $return;
    }

    public static function getPostRecord($post) {
        
        $img_id = get_post_thumbnail_id($post);

        $terms = wp_get_post_terms($post->ID, 'category' );            
        $label = $terms[0] ? $terms[0]->name : "";
        $pubdate = ['timestamp' => strtotime($post->post_date_gmt),'date' => date("d m Y", strtotime($post->post_date_gmt)),'gmt' => $post->post_date_gmt];

        $permalink = get_the_permalink($post->ID);
     
        return  [
            'id' => $post->ID,
            'title' => $post->post_title,
            'timestamp' => strtotime($post->post_date),
            'link' => get_the_permalink($post->ID),
            'excerpt' => get_the_excerpt($post->ID),
            'category' => $label, // cambiata in "label" ma lasciata perchè non sappiamo se richiamata da qualche parte
            'label' => $label,
            'pubdate' => $pubdate,
            'image' => [
                'title' => get_the_title( get_post_thumbnail_id( $post->ID ) ),
                'sizes' => Helper::getImageThumbSizes($post->ID)     
            ]
        ];
    }


    public static function moveElementInArray(&$array, $a, $b) {
        $p1 = array_splice($array, $a, 1);
        $p2 = array_splice($array, 0, $b);
        $array = array_merge($p2,$p1,$array);
        return $array;
    }

 

    public static function getNewsletter($type) {
       //ADD NEWSLETTER 
       $newsletter = "";
       if ($type == "modal") 
            $newsletter .= '<span id="newsletter-modal-top"></span>';
            if (empty($_GET['is_ajax_request']) && !is_amp_endpoint() && !is_page("newsletter-subscribe")) 
                $newsletter .= do_shortcode('[newsletter template="pi_newsletter_modal.php" authenticated="si"]');

        return[ $type => $newsletter ];
    }

    /**
     * Writes faq if present
     */
    public function getFaq($post_id){
        $faq = [];
        $faq_post = get_field("faq",$post_id);
        
        if ($faq_post)
             if (have_rows('faq_repeater',$faq_post->ID)) { 
                 
                 $faq['faq_titolo'] = $faq_post->post_title;
                 while (have_rows('faq_repeater',$faq_post->ID)) : the_row(); 
                 $faq['faq_elenco'][] = [
                         "domanda"  => get_sub_field('domanda'),
                         "risposta" => get_sub_field('risposta')
                     ];
                 endwhile; 
             }  

        // pre($faq_post,true);    
        return $faq;
     }


     
    public function getLeadGeneration($form) {
        if (!empty($form)){

            $selected = "school-areastudio=\"Che area di studio ti interessa?\" ";
            if (have_rows("mapping_aree_interesse","option")) { 
                while (have_rows("mapping_aree_interesse","option")) : the_row(); 
                    $label = get_sub_field('label');
                    while (have_rows("url_pagine","option")) : the_row(); 
                        if ($this->data['post']["link"] == get_sub_field("url_pagina")){
                            $selected = "school-areastudio=\"".$label."\" ";
                        }
                    endwhile; 
                endwhile; 
            }  
            
            $lead_generation = do_shortcode('[contact-form-7 id="' . $form->id() . '" title="' . $form->title() .' ' .$selected .']');
        }
        return $lead_generation;
        //pre($url);
        //pre($selected);
        //pre('[contact-form-7 id="' . $form->id() . '" title="' . $form->title() .'" ' .$selected.']');
        //pre($this->data['post']["lead_generation"]);
    }

}
