<?php 

/**
 * Base controller that sets basic data
 */
class Middleware {

    public $data;

    function __construct() { 
        $this->addCacheControl();
	}
 
    // set expired cache
    function addCacheControl($seconds_to_cache = '3600') {

        $headers = headers_list(); // get list of headers
        foreach ($headers as $header) { // iterate over that list of headers
            if(stripos($header,'Content-Type') !== FALSE) { // if the current header hasthe String "Content-Type" in it
                $headerParts = explode(':',$header); // split the string, getting an array
                $headerValue = trim($headerParts[1]); // take second part as value
                if (stripos($headerValue,'rss-http') !== FALSE){
                    $seconds_to_cache = "0";
                }
            }
        }

        if (empty($seconds_to_cache))  $seconds_to_cache = '3600'; // is this line  necessary ??
        if (isset($_SERVER['SERVER_NAME'])){
            $newsletter = (strpos('http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'], 'newsletter') !== false);
            $unsubscribe = (strpos('http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'], 'unsubscribe') !== false);

            if (is_admin() || 
                is_user_logged_in() ||
                $seconds_to_cache == 0 ||
                ( is_page() && ($newsletter || $unsubscribe) )
            ) {
                // don't cache 
                header("Cache-Control: no-cache, must-revalidate");
                header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
                return;
            }
               
            // cache 
            $ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
            header("Expires: $ts");
            header("Pragma: no-cache");
            header("Cache-Control: max-age=".$seconds_to_cache.", public");
        } 
    }
}
$middleWare = new Middleware();