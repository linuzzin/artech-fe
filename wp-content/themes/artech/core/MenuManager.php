<?php
/*
 *  Class MenuManager
 *  Author: F2Innovation
 *  Custom functions, Menus.
 */
Class MenuManager {
    
    /**
     * Initializer for setting up action handler
     */
    public static function init() {
        add_action('init', [ get_called_class(), 'register_menu' ] ); // Add Menu         
    }


    /**
     * Insert a new row 
     */
    public static function register_menu()
    {
        register_nav_menus([ // Using array to specify more menus if needed
            'header-menu' => 'Header Menu', // Main Navigation
            'sidebar-menu' => 'Sidebar Menu', // Sidebar Navigation
            'extra-menu' => 'Extra Menu', // Extra Navigation if needed (duplicate as many as you need!)
        ]);
    }
}
MenuManager::init();