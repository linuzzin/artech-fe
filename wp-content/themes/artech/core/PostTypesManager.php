<?php
/*
 *  Class PostTypesManager
 *  Author: F2Innovation
 *  The Posts Manager class.
 */
Class PostTypesManager {
    
    /**
     * Initializer for setting up action handler
     */
    public static function init() {
        // create standard pages 
        if (get_page_by_title('ajax') == null) Helper::createPage('ajax');    
      
        // create custom Post Types 
        add_action('init', [ get_called_class(), 'create_post_types' ] ); // Add custom post types     
        
        add_action('save_post', [ get_called_class(), 'post_save'], 10, 3);
        add_action('future_to_publish',  [ get_called_class(), 'post_save_future']);

        
    }

    
    
    /**
     * create_post_types
     */
    public static function create_post_types()
    {
    
       
        register_post_type('corsi-online', [
            'labels' => Helper::generate_labels('corsi online'),        
            'rewrite' => [ 'slug' => 'corsi-online' ],
            'public' => true, 'hierarchical' => false, 'has_archive' => true, 'show_in_rest' => true, 'can_export' => true,
            'supports' => ['title','editor','excerpt','thumbnail','author','comments'], 
            'taxonomies' => [ 'category' ]  // Add Category and Post Tags support
        ]);
        
    }
    
    
    public static function post_save_future($post_id){ 
      
    }
    public static function post_save($post_id, $post, $update){  
        
    }

}
PostTypesManager::init();