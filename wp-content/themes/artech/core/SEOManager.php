<?php
/*
 *  Class ThemeManager
 *  Author: F2Innovation
 *  Custom functions for Theme.
 */
Class SeoManager {
    
    /**
     * Initializer for setting up action handler
     */
    public static function init() {
       
        // remove unncessary header info
        add_action('init', [ get_called_class(),'remove_header_meta']);

        add_action('wpseo_add_opengraph_images', [ get_called_class(),'add_default_opengraph']);
        add_filter('wpseo_twitter_image', [ get_called_class(),'filter_wpseo_twitter_image'], 10, 1);
        add_filter('wpseo_opengraph_image_size', [ get_called_class(),'my_opengraph_image_size'], 10, 1);
        add_filter('disable_wpseo_json_ld_search', '__return_true');

        //add_action('wp_head', 'fb_move_admin_bar');
        add_action('admin_init', [ get_called_class(),'wpse151723_remove_yoast_seo_posts_filter'], 20); // Remove Yoast Seo posts filter

        add_filter( 'wpseo_robots', '__return_false' );
    }

    public static function add_default_opengraph($object)
    {
        if (is_tax('speciale')) {
            $term = get_queried_object();
            $image = get_field('immagine_copertina', $term);
            $object->add_image($image["sizes"]["hero"]);
        }
    }

    public static function my_opengraph_image_size()
    {
        return 'hero';
    }

    public static function filter_wpseo_twitter_image($image)
    {
        if (is_tax('speciale')) {
            $term = get_queried_object();
            $image = get_field('immagine_copertina', $term);
            $image = $image["sizes"]["hero"];
        }
        return $image;
    }

    public static function remove_wpseo_head_actions() {
        // If the Yoast plugin isn't installed, don't run this
        if ( ! is_callable( array( 'WPSEO_Frontend', 'get_instance' ) ) ) {
            return;
        }
    
        // Get the Yoast instantiated class
        $yoast = WPSEO_Frontend::get_instance();
    
        remove_action( 'wpseo_head', array( $yoast, 'metadesc' ), 6 );
        // per Yoast code, this is priority 10
        remove_action( 'wpseo_head', array( $yoast, 'robots' ), 10 );
        // per Yoast code, this is priority 11
        remove_action( 'wpseo_head', array( $yoast, 'metakeywords' ), 11 );
        // per Yoast code, this is priority 20
        remove_action( 'wpseo_head', array( $yoast, 'canonical' ), 20 );
        // per Yoast code, this is priority 21
        remove_action( 'wpseo_head', array( $yoast, 'adjacent_rel_links' ), 21 );
        // per Yoast code, this is priority 22
        remove_action( 'wpseo_head', array( $yoast, 'publisher' ), 22 );
    }

    public static function remove_header_meta() {
        remove_action('wp_head', 'description');
        self::remove_head_actions();
        self::remove_wpseo_head_actions();
    }
    
    
    // Remove ONLY the head actions.  Permits calling this at a "safe" time
    public static function remove_head_actions() {
        // not Yoast, but WP default. Priority is 1
        remove_action( 'wp_head', '_wp_render_title_tag', 1 );

        // If the plugin isn't installed, don't run this!
        if ( ! is_callable( [ 'WPSEO_Frontend', 'get_instance' ] ) ) {
            return;
        }

        // Get the WPSEO_Frontend instantiated class
        $yoast = WPSEO_Frontend::get_instance();
        // removed your "test" action - no need
        // per Yoast code, this is priority 0
        remove_action( 'wp_head', [ $yoast, 'front_page_specific_init' ], 0 );
        // per Yoast code, this is priority 1
        remove_action( 'wp_head', [ $yoast, 'head' ], 1 );
    }

        
    // Remove Yoast Seo posts filter
    public static function wpse151723_remove_yoast_seo_posts_filter()
    {
        global $wpseo_meta_columns;
        if ($wpseo_meta_columns) {
            remove_action('restrict_manage_posts', array($wpseo_meta_columns, 'posts_filter_dropdown'));
            remove_action('restrict_manage_posts', array($wpseo_meta_columns, 'posts_filter_dropdown_readability'));
        }
    }

    
}
SeoManager::init();

