<?php
/*
 *  Class MenuManager
 *  Author: F2Innovation
 *  Custom functions, Menus.
 */
Class ShortcodesManager {
    
    /**
     * Initializer for setting up action handler
     */
    public static function init() {

        add_shortcode('bl_youtube', [ get_called_class(), 'bl_custom_video']); // ajax load more
      

    }


    public static function bl_custom_video($args = [])
    {        
        $id_video = explode("=", $args['id'])[1];
        $url_video = "https://www.youtube.com/embed/$id_video?rel=0&amp;showinfo=0";
        
        return "
            <div class=\"embed-container\">
                <iframe src='".$url_video."' frameborder='0' allow='autoplay; encrypted-media' frameborder='0' allowfullscreen></iframe>
            </div>
        ";
    }

}
ShortcodesManager::init();

