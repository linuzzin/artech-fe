<?php
/*
 *  Class MenuManager
 *  Author: F2Innovation
 *  Custom functions, Menus.
 */
Class SidebarsManager {
    
    /**
     * Initializer for setting up action handler
     */
    public static function init() {
        SidebarsManager::register_sidebar();       
    }

    public static function register_sidebar()
    {
        // If Dynamic Sidebar Exists
        if (function_exists('register_sidebar')) {
            register_sidebar([
                'name' => __('Sidebar'),
                'description' => __('Description for this widget-area...'),
                'id' => 'sidebar',
                'before_widget' => '<div id="%1$s" class="%2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h3>',
                'after_title' => '</h3>',
            ]);
        }
    }
}
SidebarsManager::init();

