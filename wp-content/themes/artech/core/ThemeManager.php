<?php
/*
 *  Class ThemeManager
 *  Author: F2Innovation
 *  Custom functions for Theme.
 */
Class ThemeManager {
    
    /**
     * Initializer for setting up action handler
     */
    public static function init() {
        self::add_theme_support();
        

        //disabled  gutemberg editori
        add_filter('use_block_editor_for_post', '__return_false', 5);
        
        
        if( function_exists('acf_add_options_page') ) {
            
            acf_add_options_page(array(
                'page_title' 	=> 'Site Settings',
                'menu_title'	=> 'Site Settings',
                'menu_slug' 	=> 'site-general-settings',
                'capability'	=> 'edit_posts',
                'redirect'		=> false
            ));
            
        }

        // Remove Actions
        remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
        remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
        remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
        remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
        remove_action('wp_head', 'index_rel_link'); // Index link
        remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
        remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
        remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
        remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
        remove_action('wp_head', 'rel_canonical');
        remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
        remove_action('wp_head', '_wp_render_title_tag', 1 );

        // remove emojis
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('admin_print_styles', 'print_emoji_styles');


       
                
        add_filter('the_content', 'wpautop',3); //wpautop
        add_filter('the_content',  [ get_called_class(),'remove_attachment_captions'],4); // removes attachment's captions
        add_filter('the_content', [ get_called_class(),'add_image_responsive_class']);
        remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether
        add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
        add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
        add_filter('the_excerpt_rss', [ get_called_class(),'featuredtoRSS']); // display featured post thumbnails in WordPress feeds
        add_filter('the_content_feed', [ get_called_class(),'featuredtoRSS']); // display featured post thumbnails in WordPress feeds
        
        // Add Filters
        add_filter('body_class', [ get_called_class(),'add_slug_to_body_class']); // Add slug to body class (Starkers build)
        add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
        add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
        //add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
        add_filter('style_loader_tag', [ get_called_class(),'style_remove']); // Remove 'text/css' from enqueued stylesheet
        add_filter('post_thumbnail_html', [ get_called_class(),'remove_thumbnail_dimensions'], 10); // Remove width and height dynamic attributes to thumbnails
        add_filter('image_send_to_editor', [ get_called_class(),'remove_thumbnail_dimensions'], 10); // Remove width and height dynamic attributes to post images



        add_filter('wp_mail_content_type',  [ get_called_class(),'set_content_type']); // email notification filter
        add_filter('wp_mail_from',  [ get_called_class(),'wpb_sender_email']); // change email address
        add_filter('wp_mail_from_name',  [ get_called_class(),'wpb_sender_name']); // change sender name

        
        remove_all_actions('do_feed_rss');
        remove_all_actions('do_feed_rss2');
        add_action('do_feed_rss2', 'my_custom_rss', 10, 1);

        add_action( 'init', 'remove_wc_page_noindex' );
    }
    
    public static function add_theme_support()
    {
        if (function_exists('add_theme_support')) {
            
            // Add Menu Support
            add_theme_support('menus');
        
        
            // Add Custom Thumbnail Theme Support
            add_theme_support('post-thumbnails');
               
            
            
            // Enables post and comment RSS feed links to head
            add_theme_support('automatic-feed-links');
            // Remove first p in single content
            //remove_filter('the_content', 'wpautop');
        
            // Localisation Support
            //load_theme_textdomain('newstreet', get_template_directory().'/languages');
        
                
            add_post_type_support( 'page', 'excerpt' );
        }
    }

    // wraps every iframe up with a specific class 'embed-container'
    public static function remove_attachment_captions($content)
    {
        // Removes wp-caption
        preg_match_all('/\[caption id="attachment_.*?" align=".*" width=".*?"\]<img (.*?)>(.*?)\[\/caption\]/', $content, $matches);

        foreach ($matches[0] as $key => $snippet) {
            $replace = '<img ' . $matches[1][$key] . '><div class="img-caption">' . $matches[2][$key] . '</div>';
            $content = str_replace($snippet, $replace, $content);
        }

        return $content;
    }

    
    public static function add_image_responsive_class($content)
    {
        global $post;
        $pattern = "/<img(.*?)class=\"(.*?)\"(.*?)>/i";
        $replacement = '<img$1class="$2 img-responsive content-image"$3>';
        $content = preg_replace($pattern, $replacement, $content);
        return $content;
    }

    // Remove 'text/css' from our enqueued stylesheet
    public static function style_remove($tag)
    {
        return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
    }


    // Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
    public static function remove_thumbnail_dimensions($html)
    {
        $html = preg_replace('/(width|height)=\"\d*\"\s/', '', $html);

        return $html;
    }

    // Function to change email address
    public static function wpb_sender_email($original_email_address)
    {
        return WP_EMAIL;
    }

    
    // Function to change sender name
    public static function wpb_sender_name($original_email_from)
    {
        return 'Artech';
    }
    // email notification filter
    public static function set_content_type($content_type)
    {
        return 'text/html';
    }

    // display featured post thumbnails in WordPress feeds
    public static function featuredtoRSS($content)
    {
        global $post;
        if (has_post_thumbnail($post->ID)) {
            $content = '<p>' . get_the_post_thumbnail($post->ID) . '</p>' . $content;
        }
        return $content;
    }

    
    // Add page slug to body class, love this - Credit: Starkers Wordpress Theme
    public static function add_slug_to_body_class($classes)
    {
        global $post;
        if (is_home()) {
            $key = array_search('blog', $classes);
            if ($key > -1) {
                unset($classes[$key]);
            }
        } elseif (is_page()) {
            $classes[] = sanitize_html_class($post->post_name);
        } elseif (is_singular()) {
            $classes[] = sanitize_html_class($post->post_name);
        }

        return $classes;
    }


    
}
ThemeManager::init();

