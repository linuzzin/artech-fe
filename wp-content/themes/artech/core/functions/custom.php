<?php
/*
 *  Author: F2Innovation
 *  Custom functions.
 */
 // apache_request_headers replicement for nginx
 if (!function_exists('apache_request_headers')) { 
    function apache_request_headers() { 
        $return = array();
        foreach($_SERVER as $key=>$value) { 
            if (substr($key,0,5)=="HTTP_") { 
                $key=str_replace(" ","-",ucwords(strtolower(str_replace("_"," ",substr($key,5))))); 
                $return[strtolower($key)]=$value; 
            }else{
                $return[strtolower($key)]=$value; 
            }
        } 
        return $return; 
    } 
}


// custom print_r function
function pre($text, $stop = false) {
    echo "<pre>";
    print_r($text);
    echo "</pre>";
    if ($stop) exit;
}

/** 
 * Replace special characters and spaces in a given string
 * and return the result lowering capital letters
 */
function slugify($text) {

    $text = str_replace('à', 'a', $text);
    $text = str_replace(array('è','é'), 'e', $text);
    $text = str_replace('ì', 'i', $text);
    $text = str_replace('ò', 'o', $text);
    $text = str_replace('ù', 'u', $text);
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    
    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);
    // trim
    $text = trim($text, '-');
    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);
    // lowercase
    $text = strtolower($text);
    if (empty($text)) {
        return 'n-a';
    }
    return $text;
}


/**
 * Returns the type of the current page 
 */
function check_page() {

    global $wp_query;
    //pre($wp_query);
    $loop = 'notfound';

    if ($wp_query->is_page) {
        $loop = is_front_page() ? 'front' : 'page';
    } elseif ($wp_query->is_home) {
        $loop = 'home';
    } elseif ($wp_query->is_single) {
        $loop = ($wp_query->is_attachment) ? 'attachment' : 'single';
    } elseif ($wp_query->is_category) {
        $loop = 'category';
    } elseif ($wp_query->is_tag) {
        $loop = 'tag';
    } elseif ($wp_query->is_tax) {
        $loop = 'tax';
    } elseif ($wp_query->is_archive) {
        if ($wp_query->is_day) {
            $loop = 'day';
        } elseif ($wp_query->is_month) {
            $loop = 'month';
        } elseif ($wp_query->is_year) {
            $loop = 'year';
        } elseif ($wp_query->is_author) {
            $loop = 'author';
        } else {
            $loop = 'archive';
        }
    } elseif ($wp_query->is_search) {
        $loop = 'search';
    } elseif ($wp_query->is_404) {
        $loop = 'notfound';
    }

    $output = $loop;

    if (!empty($wp_query->query_vars['post_type'])){
        if (is_array($wp_query->query_vars['post_type'])) {
            $output = implode("-", $wp_query->query_vars['post_type']) . "-" .$loop;
        }else{
            $output = $wp_query->query_vars['post_type'] . "-" .$loop;
        }
    }
    return  $output;
}



// Determine the top-most parent of a term
function get_term_top_most_parent( $term, $taxonomy ) {
    // Start from the current term
    $parent  = get_term( $term, $taxonomy );
    // Climb up the hierarchy until we reach a term with parent = '0'
    while ( $parent->parent != '0' ) {
        $term_id = $parent->parent;
        $parent  = get_term( $term_id, $taxonomy);
    }
    return $parent;
}


function printTimestamp($datetime) { 
    $date = new DateTime($datetime);
    return $date->getTimestamp();
}

 // Function to get the client IP address
function get_client_ip() {

    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}