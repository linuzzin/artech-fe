<?php
/**
 * 
 * 
 */
Class CF7Manager  {

    private static $lead_table = 'pi_whitepaper_leads';
    
    /**
     * Initializer for setting up action handler
     */
    public static function init() {
        add_filter( 'wpcf7_load_js', '__return_false' );
        add_filter( 'acf_cf7_object', '__return_true' );
        add_filter('cfdb7_after_save_data',[ get_called_class(), 'custom_cfdb7_after_save_data'], 10, 1 );

    }

    function custom_cfdb7_after_save_data($insert_id){
       // self::sendingblu_save(); 
    }
}

CF7Manager::init();