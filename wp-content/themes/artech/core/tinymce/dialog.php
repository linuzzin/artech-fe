<?php
if (!defined('ABSPATH')) {
    die("Can't load this file directly");
}
@header('Content-Type: ' . get_option('html_type') . '; charset=' . get_option('blog_charset'));
?>
<!doctype html>
<html lang="it">
    <head>
        <meta charset="utf-8" />
        <title>Inserisci Video e Gallerie</title>
        <link rel='stylesheet' id='jquery-ui-css-css'  href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/flick/jquery-ui.css?ver=3.4.2' type='text/css' media='all' />
        <link rel='stylesheet' id='jquery-ui-css-css'  href='https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.css'></script>
        
        <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js'></script>
        <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js'></script>  
        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.min.js'></script>
        
        <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/scripts/admin/select2.v3.full.min.js'></script>
        <script language="javascript" type="text/javascript" src="<?php echo site_url(); ?>/wp-includes/js/tinymce/tiny_mce_popup.js"></script>

        <script type='text/javascript'>

        </script>
        <style type="text/css">
            body {
                font-size: 13px;
            }
            .ui-widget .ui-widget {
                font-size: 12px;
            }
            .ui-corner-top {
                font-size: 13px;
            }
        </style>
    </head>
    <body>

<div id="tabs">
    <ul>
        <li><a href="#tabs-2">Gallery</a></li>
        <li><a href="#tabs-3">Video</a></li>
    </ul>
    <div id="tabs-2">
        <div id="pigaltogal-form">
            
                
             
            <fieldset style="padding: 4px 20px;">
                <legend>Gallerie</legend><br>
                <div class="form-group">
                    <input type="text" id="id_gallery" name="s" class="form-control search-autocomplete" style="width: 100%; padding: 4px"  data-posttype="immagini" placeholder="Search">
                </div>
            </fieldset>

            <p class="submit" style="padding: 15px 0;">
                <input type="submit" class="close"  data-type="gallery" class="button-primary" value="Inserisci" name="submit" />
            </p>
        </div>
    </div>
    <div id="tabs-3">
        <div id="pivideo-form">
            <fieldset style="padding: 4px 20px;">
                <legend>Video</legend><br>
                <div class="form-group">
                    <input type="text"  id="id_video" name="s" class="form-control search-autocomplete" style="width: 100%; padding: 4px"  data-posttype="video" placeholder="Search">
                </div>
            </fieldset>
            <p class="submit" style="padding: 15px 0;">
                <input type="submit" class="close" data-type="video" class="button-primary" value="Inserisci" name="submit" />
            </p>
        </div>
    </div>
</div>



<script>
    var arr_results;
    var searchRequest;
    var action = 'search';
    var posttype;
    jQuery(document).ready(function($) { 
        $( "#tabs" ).tabs();

        $( "input[type=submit]" )
        .button()
        .click(function( event ) {
            event.preventDefault();
        });
        
        $('.search-autocomplete').on('focus',function(){
            posttype = $(this).data('posttype');
            action = 'search_'+posttype;
        });
        $('.search-autocomplete').autoComplete({
            minChars: 2,
            source: function(term, suggest){
                try { searchRequest.abort(); } catch(e){}
                searchRequest = $.post('<?php echo admin_url('admin-ajax.php'); ?>', { search: term, action: action }, function(res) {
                    suggest(res.data.map(a => a.title));
                    arr_results = res.data;
                });
            }
        });
        $('.close').click(function(){
            
            var type_input = $(this).data('type');
            var value = jQuery("#id_"+type_input).val();
            for (i = 0; i < arr_results.length; i++) { 
                if (arr_results[i]["title"] == value) {
                    console.log(arr_results[i]);
                    var shortcode = '['+type_input+'_embed';            
                    shortcode += ' ' + 'id' + '=' + arr_results[i]["ID"];
                    shortcode += ']';
                }
            }

            // inserts the shortcode into the active editor
            tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode);
        
            // closes Thickbox
            tinyMCEPopup.close();
            
            
        });
        
    });

    
</script>
</body>
</html>