<?php
/*
 *  Author: F2 Innovation | @Emanuele Gennuso
 *  URL: pi.com | @pi
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
    External Modules/Files
\*------------------------------------*/

// load CLASS
require 'class/Helper.php';
require 'class/Middleware.php';


// load CORE
require 'core/MenuManager.php';
require 'core/ThemeManager.php';
require 'core/SEOManager.php';
require 'core/TaxonomiesManager.php';
require 'core/PostTypesManager.php';
require 'core/RewritesManager.php';
require 'core/SidebarsManager.php';
require 'core/ShortcodesManager.php';
require 'core/WidgetsManager.php';
// Load functions
require 'core/functions/custom.php';
//Load Hook
require 'core/hooks/CF7Manager.php';


